package cn.cocho.dborm.util;

import java.io.File;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

/**
 * 数据库连接相关的信息管理接口
 * 
 * @author dborm@cocho
 * @time 2014年1月15日 @下午5:08:18
 */
public class DbormDataBase {

	/**
	 * 获得数据库连接
	 * 
	 * @return 数据库连接
	 * @author dborm@cocho
	 * @time 2013-5-6上午10:46:44
	 */
	public SQLiteDatabase getConnection() {
		String dbFilePath = DbormContexts.getDbFilePath();
		if (StringUtilsDborm.isBlank(dbFilePath)) {
			throw new RuntimeException("数据库路径不能为空，请使用DbormContexts.setDbFilePath（[path]）设置.");
		}
		File dbFile = new File(DbormContexts.getDbFilePath());
		if (!dbFile.exists()) {
			new File(dbFile.getParent()).mkdirs();
		}
		SQLiteDatabase.loadLibs(DbormContexts.context);
		SQLiteOpenHelper dbHelper = new DatabaseHelperSqlcipher(DbormContexts.context, dbFilePath, null, 1);
        return dbHelper.getWritableDatabase(DbormContexts.dbPassword);
	}

	/**
	 * 新曾对象操作之前
	 * 
	 * @param entity
	 *            实体对象
	 * @param <T>
	 *            对象类型
	 * @return 处理之后的对象
	 */
	public <T> T beforeInsert(T entity) {
		return entity;
	}

	/**
	 * 修改对象操作之前
	 * 
	 * @param entity
	 *            实体对象
	 * @param <T>
	 *            对象类型
	 * @return 处理之后的对象
	 */
	public <T> T beforeUpdate(T entity) {
		return entity;
	}

	/**
	 * 删除对象操作之前
	 * 
	 * @param entity
	 *            实体对象
	 * @param <T>
	 *            对象类型
	 * @return 处理之后的对象
	 */
	public <T> T beforeDelete(T entity) {
		return entity;
	}

	/**
	 * 替换队形操作之前
	 * 
	 * @param entity
	 *            实体对象
	 * @param <T>
	 *            对象类型
	 * @return 处理之后的对象
	 */
	public <T> T beforeReplace(T entity) {
		return entity;
	}

	/**
	 * 关闭数据库链接
	 * 
	 * @param database
	 *            数据库链接
	 */
	public void closeConn(SQLiteDatabase database) {
		if (database != null) {
			try {
				database.close();
			} catch (Exception ignored) {
			}
		}
	}

}
