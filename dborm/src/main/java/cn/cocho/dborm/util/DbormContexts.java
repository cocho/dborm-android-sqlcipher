package cn.cocho.dborm.util;

import java.io.File;

import android.content.Context;

/**
 * 框架环境变量
 *
 * @author dborm@cocho
 * @time 2013-5-6上午11:58:07
 */
public class DbormContexts {


    public static Context context;

    /**
     * 是否显示SQL语句（必须在DEBUG模式下并且异常处理类被实现了该参数才有效）
     */
    public static boolean showSql = true;

    /**
     * 日志记录工具类
     */
    public static LoggerDborm log = null;


    /**
     * 当前dborm的版本（初始化dborm的时候会根据该版本号决定是否要重建数据库）
     */
    public static int dbVersion = 1;

    /**
     * 表结构信息的文件路径((在assets目录下的相对位置，目录尾不能添加/)
     */
    public static String schemaPath = "db" + File.separator + "schema";

    /**
     * 数据库版本升级的脚本文件位置(在assets目录下的相对位置)
     */
    public static String versionPath = "db" + File.separator + "version" + File.separator;

    /**
     * 数据库密码
     */
    public static String dbPassword = ".123456";

    /**
     * 数据库存放路径（包含数据库名称）
     */
    private static String dbFilePath;

    /**
     * 获得数据库文件的路径
     *
     * @return 数据库路径
     * @author HAIKANG SONG
     * @time 2013-5-2下午4:45:10
     */
    public static String getDbFilePath() {
        return dbFilePath;
    }

    public static void setDbFilePath(String dbFilePath) {
        DbormContexts.dbFilePath = dbFilePath;
    }


}
