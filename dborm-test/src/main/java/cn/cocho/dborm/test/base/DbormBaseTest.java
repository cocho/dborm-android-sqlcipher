package cn.cocho.dborm.test.base;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;
import cn.cocho.dborm.core.Dborm;
import cn.cocho.dborm.core.DbormInit;
import cn.cocho.dborm.util.DbormContexts;
import cn.cocho.dborm.util.DbormDataBase;
import cn.cocho.dborm.utils.LoggerTools;
import cn.cocho.dborm.utils.MainActivity;
import cn.cocho.dborm.utils.domain.LoginUser;
import cn.cocho.dborm.utils.domain.QsmInfo;
import cn.cocho.dborm.utils.domain.QsmOption;

public class DbormBaseTest extends ActivityInstrumentationTestCase2<MainActivity> {

	public DbormBaseTest() {
		super("cn.cocho.dborm.test", MainActivity.class);
	}

	/**
	 * 该函数在每个测试函数执行的时候都会调用
	 */
	protected void setUp() throws Exception {
		super.setUp();
		initContexts();
		try {
			Dborm.setDataBase(new DbormDataBase());
			initDb();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void initContexts() {
		DbormContexts.showSql = true;
		DbormContexts.log = new LoggerTools();
		DbormContexts.dbVersion = 1;
		DbormContexts.setDbFilePath(Environment.getExternalStorageDirectory() + File.separator + "tbc" + File.separator + "db"
                + File.separator + "db_test.db");
	}

	public void initDb() throws Exception {
		File oldFile = new File(DbormContexts.getDbFilePath());
		oldFile.delete();
		new DbormInit().initDb(this.getActivity());
		File file = new File(DbormContexts.getDbFilePath());
		assertEquals(true, file.exists());
	}

	public void initData() {
		List<LoginUser> users = new ArrayList<LoginUser>();
		for (int i = 0; i < 5; i++) {
			LoginUser user = new LoginUser();
			user.setId("INIT-ID-" + i);
			user.setUserId("INIT-USER-ID-" + i);
			user.setUserName("Tom" + i);
			user.setAge(i);
			user.setIsBoy(true);
			user.setBirthday(new Date());
			user.setLoginNum(i);
			users.add(user);
		}
		assertEquals(true, Dborm.insert(users));

		List<QsmInfo> infos = new ArrayList<QsmInfo>();
		for (int i = 0; i < 5; i++) {
			QsmInfo info = new QsmInfo();
			info.setUserId("INIT-USER-ID-" + i);
			info.setQuestionId("INIT-QUESION-ID-" + i);
			info.setContent("描述信息" + i);
			infos.add(info);
		}
		assertEquals(true, Dborm.insert(infos));
		
		List<QsmOption> options = new ArrayList<QsmOption>();
		for (int i = 0; i < 5; i++) {
			QsmOption option = new QsmOption();
			option.setUserId("INIT-USER-ID-" + i);
			option.setOptionId("INIT-OPTION-ID-"+i);
			option.setAttachmentCount(i);
			option.setContent("描述信息" + i);
			option.setQuestionId("INIT-QUESION-ID-" + i);
			option.setShowOrder(i+10f);
			options.add(option);
		}
		assertEquals(true, Dborm.insert(options));

	}
	
	

}
