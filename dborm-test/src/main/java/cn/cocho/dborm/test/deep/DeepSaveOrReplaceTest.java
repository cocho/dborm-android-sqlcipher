package cn.cocho.dborm.test.deep;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.cocho.dborm.core.Dborm;
import cn.cocho.dborm.test.base.DbormBaseTest;
import cn.cocho.dborm.utils.domain.LoginUser;
import cn.cocho.dborm.utils.domain.QsmOption;

public class DeepSaveOrReplaceTest extends DbormBaseTest {

	int nums = 3;

	public void initTestData() {
		LoginUser user = new LoginUser();
		user.setId("relation111");
		user.setUserId("relation1");
		user.setAge(10);
		user.setBirthday(new Date());
		List<QsmOption> optionList = new ArrayList<QsmOption>();
		for (int i = 0; i < nums; i++) {
			QsmOption option = new QsmOption();
			option.setOptionId("optionId" + i);
			option.setQuestionId("questionId111");
			option.setContent("测试");
			optionList.add(option);
		}
		user.setQsmOptionList(optionList);
		List<LoginUser> users = new ArrayList<LoginUser>();
		users.add(user);
		boolean result = Dborm.saveOrReplace(users);
		assertEquals(true, result);
	}

	public void testB15SaveOrReplace() {
		initTestData();
		assertEquals(1, Dborm.getEntityCount(LoginUser.class));
		assertEquals(nums, Dborm.getEntityCount(QsmOption.class));
	}

	public void testB20SaveOrReplace() {
		initTestData();
		LoginUser user = new LoginUser();
		user.setId("relation111");
		user.setUserId("relation1");
		user.setAge(20);
		List<QsmOption> optionList = new ArrayList<QsmOption>();
		for (int i = 0; i < nums; i++) {
			QsmOption option = new QsmOption();
			option.setOptionId("optionId-replace" + i);// 主键发生变化，将变为新增操作
			option.setQuestionId("questionId111");
			option.setContent("测试");
			optionList.add(option);
		}
		user.setQsmOptionList(optionList);
		List<LoginUser> users = new ArrayList<LoginUser>();
		users.add(user);
		boolean result = Dborm.saveOrReplace(users);
		assertEquals(true, result);
		assertEquals(1, Dborm.getEntityCount(LoginUser.class));
		assertEquals(nums * 2, Dborm.getEntityCount(QsmOption.class));
	}

}
