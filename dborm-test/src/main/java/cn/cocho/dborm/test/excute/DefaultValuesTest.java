package cn.cocho.dborm.test.excute;

import java.util.Date;
import java.util.List;

import cn.cocho.dborm.core.Dborm;
import cn.cocho.dborm.test.base.DbormBaseTest;
import cn.cocho.dborm.utils.domain.LoginUser;

/**
 * 测试数据库字段中使用默认值
 * 
 * @author HAIKANG SONG
 * @2013年8月6日 @下午6:06:30
 */
public class DefaultValuesTest extends DbormBaseTest {
	
	
	public void testB11Insert(){
		LoginUser user = new LoginUser();
		user.setId("ID1");
		user.setUserId("USID1");
		user.setUserName("Tom");
		user.setAge(10);
		user.setBirthday(new Date());
		boolean result = Dborm.insert(user);
		assertEquals(true, result);
		
		List<LoginUser> userList = Dborm.getEntities("select * from login_user where id='ID1'", null, LoginUser.class);
		LoginUser queryUser = userList.get(0);
		Integer loginNum = queryUser.getLoginNum();
		if(loginNum != null){
			int num = loginNum;
			assertEquals(0, num);//因为schema（表的描述文件）中设置了默认值为0
		}else{
			assertEquals(true, false);
		}
	}
	
	
	
	
	

}
