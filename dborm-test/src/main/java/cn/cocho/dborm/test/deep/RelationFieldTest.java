package cn.cocho.dborm.test.deep;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.cocho.dborm.core.Dborm;
import cn.cocho.dborm.test.base.DbormBaseTest;
import cn.cocho.dborm.utils.domain.LoginUser;
import cn.cocho.dborm.utils.domain.QsmOption;

public class RelationFieldTest extends DbormBaseTest {

	int nums = 3;

	public void initTestData() {
		LoginUser user = new LoginUser();
		user.setId("relation111");
		user.setUserId("relation1");
		user.setAge(10);
		user.setUserName("Jace");
		user.setBirthday(new Date());
		List<QsmOption> optionList = new ArrayList<QsmOption>();
		for (int i = 0; i < nums; i++) {
			QsmOption option = new QsmOption();
			option.setOptionId("optionId" + i);
			option.setQuestionId("questionId111");
			option.setContent("测试");
			optionList.add(option);
		}
		user.setQsmOptionList(optionList);
		boolean result = Dborm.insert(user);
		assertEquals(true, result);
	}

	public void testB10Insert() {
		initTestData();
		assertEquals(1, Dborm.getEntityCount(LoginUser.class));
		assertEquals(nums, Dborm.getEntityCount(QsmOption.class));
	}

	public void testB15Update() {
		initTestData();
		LoginUser user = new LoginUser();
		user.setId("relation111");
		user.setUserId("relation1");
		user.setUserName("JaceUpdate");
		user.setBirthday(new Date());
		List<QsmOption> optionList = new ArrayList<QsmOption>();
		for (int i = 0; i < nums; i++) {
			QsmOption option = new QsmOption();
			option.setOptionId("optionId" + i);
			option.setQuestionId("questionId111");
			option.setContent("测试修改");
			optionList.add(option);
		}
		user.setQsmOptionList(optionList);
		boolean result = Dborm.update(user);
		assertEquals(true, result);
		
		LoginUser user2 = Dborm.getEntity("select * from login_user where id='relation111'", null, LoginUser.class);
		assertEquals("JaceUpdate", user2.getUserName());
		Integer age = user2.getAge();
		assertEquals(null, age);//update修改所有属性的值，未指明值的属性将会修改为null，所以age属性未指明值，修改之后应该为null
		
		QsmOption option = Dborm.getEntity("select * from qsm_option where option_id='optionId1'", null, QsmOption.class);
		assertEquals("测试修改", option.getContent());
	}

	public void testB20Replace() {
		initTestData();
		LoginUser user = new LoginUser();
		user.setId("relation111");
		user.setUserId("relation1");
		user.setAge(30);
		user.setBirthday(new Date());
		List<QsmOption> optionList = new ArrayList<QsmOption>();
		for (int i = 0; i < nums; i++) {
			QsmOption option = new QsmOption();
			option.setOptionId("optionId" + i);
			option.setQuestionId("questionId111");
			option.setContent("测试替换");
			optionList.add(option);
		}
		user.setQsmOptionList(optionList);
		boolean result = Dborm.replace(user);
		assertEquals(true, result);
		
		QsmOption option = Dborm.getEntity("select * from qsm_option where option_id='optionId1'", null, QsmOption.class);
		assertEquals("测试替换", option.getContent());

	}

	public void testB10Delete() {
		initTestData();
		LoginUser user = new LoginUser();
		user.setId("relation111");
		user.setUserId("relation1");
		user.setAge(10);
		user.setBirthday(new Date());
		List<QsmOption> optionList = new ArrayList<QsmOption>();
		for (int i = 0; i < 10; i++) {
			QsmOption option = new QsmOption();
			option.setOptionId("optionId" + i);
			option.setQuestionId("questionId111");
			option.setContent("测试");
			optionList.add(option);
		}
		user.setQsmOptionList(optionList);
		boolean result = Dborm.delete(user);
		assertEquals(true, result);
		
		assertEquals(0, Dborm.getEntityCount(LoginUser.class));
		assertEquals(0, Dborm.getEntityCount(QsmOption.class));
	}

}
