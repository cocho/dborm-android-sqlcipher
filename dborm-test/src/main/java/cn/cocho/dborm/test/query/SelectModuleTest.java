package cn.cocho.dborm.test.query;

import java.util.ArrayList;
import java.util.List;

import cn.cocho.dborm.core.Dborm;
import cn.cocho.dborm.test.base.DbormBaseTest;
import cn.cocho.dborm.utils.domain.LoginUser;
import cn.cocho.dborm.utils.domain.QsmOption;
import cn.cocho.dborm.utils.domain.SelectModule;

/**
 * 使用查询模版的测试用例
 * 
 * @author HAIKANG SONG
 * @2013年7月27日 @下午2:59:43
 */
public class SelectModuleTest extends DbormBaseTest {

	
	private final String USER_ID = "23432423asdasdq321eada";
	private final String USER_NAME = "Tom";
	private final int USER_AGE = 20;

	private final String QSM_CONTENT = "测试内容";
	
	

	public void initTestData() {
		LoginUser user = new LoginUser();
		user.setId("dsfdsfsdafdsfds2343sdfsdf");
		user.setUserId(USER_ID);
		user.setUserName(USER_NAME);
		user.setAge(USER_AGE);

		List<QsmOption> qsmOptionList = new ArrayList<QsmOption>();
		for (int i = 0; i < 1; i++) {
			QsmOption option = new QsmOption();
			option.setOptionId("OPTION_ID_" + i);
			option.setQuestionId("789");
			option.setContent(QSM_CONTENT);
			option.setUserId(USER_ID);
			option.setShowOrder(i + 10f);
			qsmOptionList.add(option);
		}
		user.setQsmOptionList(qsmOptionList);

		boolean result = Dborm.insert(user);
		assertEquals(true, result);
	}

	/**
	 * 测试连接查询
	 * 
	 * @author HAIKANG SONG
	 * @time 2013-6-6下午5:45:59
	 */
	public void testB25GetJoinEntitys() {
		initTestData();
		String sql = "SELECT u.user_id, u.user_name, q.question_id, q.content, q.show_order FROM qsm_option q LEFT JOIN login_user u ON u.user_id=q.user_id WHERE u.user_id = ? ";
		String[] bindArgs = new String[] { USER_ID };
		List<SelectModule> moduleList = Dborm.getEntities(sql, bindArgs, SelectModule.class);
		for (int i = 0; i < bindArgs.length; i++) {
			SelectModule module = moduleList.get(i);
			assertEquals(USER_NAME, module.getUserName());
			assertEquals(QSM_CONTENT, module.getContent());
		}
	}

	public void testB28GetJoinEntitys() {
		initTestData();
		String sql = "SELECT * FROM qsm_option q LEFT JOIN login_user u ON u.user_id=q.user_id WHERE u.user_id = ? ";
		String[] bindArgs = new String[] { USER_ID };
		List<SelectModule> moduleList = Dborm.getEntities(sql, bindArgs, SelectModule.class);
		for (int i = 0; i < bindArgs.length; i++) {
			SelectModule module = moduleList.get(i);
			assertEquals(USER_NAME, module.getUserName());
			assertEquals(QSM_CONTENT, module.getContent());
		}
	}
}
