package cn.cocho.dborm.test.excute;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.cocho.dborm.core.Dborm;
import cn.cocho.dborm.test.base.DbormBaseTest;
import cn.cocho.dborm.utils.domain.LoginUser;


/**
 * 对象的批量操作
 * 
 * @author HAIKANG SONG
 * @2013年7月27日 @下午3:05:06
 */
public class ListEntityTest extends DbormBaseTest {

	int nums = 3;//记录条数
	
	
	final int ages = 10;
	
	public void initTestData(){
		List<LoginUser> userList = new ArrayList<LoginUser>();
		for (int i = 0; i < nums; i++) {
			LoginUser user = new LoginUser();
			user.setId("id"+i);
			user.setUserId("userId"+i);
			user.setUserName("Tom");
			user.setAge(ages);
			user.setBirthday(new Date());
			userList.add(user);
		}
		boolean result = Dborm.insert(userList);
		assertEquals(true, result);
	}
	
	public void testC10InsertList(){
		initTestData();
		int num = Dborm.getEntityCount(LoginUser.class);
		assertEquals(nums, num);
	}
	
	public void testC15ReplaceList(){
		initTestData();
		List<LoginUser> userList = new ArrayList<LoginUser>();
		for (int i = 0; i < nums; i++) {
			LoginUser user = new LoginUser();
			user.setId("id"+i);
			user.setUserId("userId"+i);
			user.setUserName("TomReplaceToLucy");
			userList.add(user);
		}
		boolean result = Dborm.replace(userList);
		assertEquals(true, result);
		
		LoginUser user = Dborm.getEntity("select * from login_user where id='id0'", null, LoginUser.class);
		assertEquals("TomReplaceToLucy", user.getUserName());
		int age = user.getAge();
		assertEquals(ages, age);//replace修改属性值不为null的字段，age属性未指明值，所以replace之后仍然和之前一样
	}
	
	public void testC20UpdateList(){
		initTestData();
		List<LoginUser> userList = new ArrayList<LoginUser>();
		for (int i = 0; i < nums; i++) {
			LoginUser user = new LoginUser();
			user.setId("id"+i);
			user.setUserId("userId"+i);
			user.setUserName("LucyUpdateToJack");
			user.setBirthday(new Date());
			userList.add(user);
		}
		boolean result = Dborm.update(userList);
		assertEquals(true, result);
		
		LoginUser user = Dborm.getEntity("select * from login_user where id='id0'", null, LoginUser.class);
		assertEquals("LucyUpdateToJack", user.getUserName());
		Integer age = user.getAge();
		assertEquals(null, age);//update修改所有属性的值，未指明值的属性将会修改为null，所以age属性未指明值，修改之后应该为null
		
	}
	
	public void testC30DeleteList(){
		initTestData();
		List<LoginUser> userList = new ArrayList<LoginUser>();
		for (int i = 0; i < nums; i++) {
			LoginUser user = new LoginUser();
			user.setId("id"+i);
			user.setUserId("userId"+i);
			userList.add(user);
		}
		boolean result = Dborm.delete(userList);
		assertEquals(true, result);
		
		int num = Dborm.getEntityCount(LoginUser.class);
		assertEquals(0, num);
		
	}
	
	
	
	
	
	
	
	
	

}
