package cn.cocho.dborm.test.excute;

import java.util.Date;

import cn.cocho.dborm.core.Dborm;
import cn.cocho.dborm.test.base.DbormBaseTest;
import cn.cocho.dborm.utils.domain.LoginUser;

public class SigleEntityTest extends DbormBaseTest {

	final int ages = 10;
	
	public void initTestData(){
		LoginUser user = new LoginUser();
		user.setId("ID1");
		user.setUserId("USID1");
		user.setUserName("Tom");
		user.setAge(ages);
		user.setBirthday(new Date());
		boolean result = Dborm.insert(user);
		assertEquals(true, result);
	}
	
	public void testB11Insert(){
		initTestData();
		int num = Dborm.getEntityCount(LoginUser.class);
		assertEquals(1, num);
	}
	
	public void testB15Replace(){
		initTestData();
		LoginUser user = new LoginUser();
		user.setId("ID1");
		user.setUserId("USID1");
		user.setUserName("TomReplace");
		boolean result = Dborm.replace(user);
		assertEquals(true, result);
		
		LoginUser user2 = Dborm.getEntity("select * from login_user where id='ID1'", null, LoginUser.class);
		assertEquals("TomReplace", user2.getUserName());
		int age = user2.getAge();
		assertEquals(ages, age);//replace修改属性值不为null的字段，age属性未指明值，所以replace之后仍然和之前一样
	}
	
	public void testB20Update(){
		initTestData();
		LoginUser user = new LoginUser();
		user.setId("ID1");
		user.setUserId("USID1");
		user.setUserName("TomUpdate");
		user.setBirthday(new Date());
		boolean result = Dborm.update(user);
		assertEquals(true, result);
		
		LoginUser user2 = Dborm.getEntity("select * from login_user where id='ID1'", null, LoginUser.class);
		assertEquals("TomUpdate", user2.getUserName());
		Integer age = user2.getAge();
		assertEquals(null, age);//update修改所有属性的值，未指明值的属性将会修改为null，所以age属性未指明值，修改之后应该为null
		
	}
	
	public void testB35Delete(){
		initTestData();
		int num = Dborm.getEntityCount(LoginUser.class);
		assertEquals(1, num);
		
		LoginUser user = new LoginUser();
		user.setId("ID1");
		user.setUserId("USID1");
		boolean result = Dborm.delete(user);
		assertEquals(true, result);
		
		int num2 = Dborm.getEntityCount(LoginUser.class);
		assertEquals(0, num2);
	}
	
	public void testD10SaveOrUpdate(){
		initTestData();
		LoginUser user = new LoginUser();
		user.setId("ID2");
		user.setUserId("USID2");
		user.setUserName("Tom");
		boolean result = Dborm.saveOrUpdate(user);
		assertEquals(true, result);
		
		int num2 = Dborm.getEntityCount(LoginUser.class);
		assertEquals(2, num2);//因为初始化数据时使用的ID是ID1，本次操作使用的是ID2，主键不同，所以为新增操作
		
	}
	
	public void testD15SaveOrUpdate(){
		initTestData();
		LoginUser user = new LoginUser();
		user.setId("ID1");
		user.setUserId("USID1");
		user.setUserName("TomSaveOrUpdate");
		boolean result = Dborm.saveOrUpdate(user);
		assertEquals(true, result);
		
		LoginUser user2 = Dborm.getEntity("select * from login_user where id='ID1'", null, LoginUser.class);
		assertEquals("TomSaveOrUpdate", user2.getUserName());//因为初始化数据时使用的主键和当前主键相同所以为修改操作
		Integer age = user2.getAge();
		assertEquals(null, age);//update修改所有属性的值，未指明值的属性将会修改为null，所以age属性未指明值，修改之后应该为null
	}
	
	public void testD20SaveOrReplace(){
		initTestData();
		LoginUser user = new LoginUser();
		user.setId("ID2");
		user.setUserId("USID2");
		user.setAge(10);
		boolean result = Dborm.saveOrReplace(user);
		assertEquals(true, result);
		
		int num2 = Dborm.getEntityCount(LoginUser.class);
		assertEquals(2, num2);//因为初始化数据时使用的ID是ID1，本次操作使用的是ID2，主键不同，所以为新增操作
	}
	
	
	
	
	
	
	
	

}
