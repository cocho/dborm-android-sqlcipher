package cn.cocho.dborm.test.query;

import java.util.ArrayList;
import java.util.List;

import net.sqlcipher.Cursor;
import android.database.SQLException;
import net.sqlcipher.database.SQLiteDatabase;
import cn.cocho.dborm.core.Dborm;
import cn.cocho.dborm.test.base.DbormBaseTest;
import cn.cocho.dborm.util.LoggerUtilsDborm;
import cn.cocho.dborm.utils.domain.LoginUser;
import cn.cocho.dborm.utils.domain.QsmOption;

public class SelectTest extends DbormBaseTest {

	private final String USER_ID = "23432423asdasdq321eada";
	private final String USER_NAME = "Tom";
	private final int USER_AGE = 20;

	private final String QSM_CONTENT = "测试内容";

	public void initTestData() {
		LoginUser user = new LoginUser();
		user.setId("dsfdsfsdafdsfds2343sdfsdf");
		user.setUserId(USER_ID);
		user.setUserName(USER_NAME);
		user.setAge(USER_AGE);

		List<QsmOption> qsmOptionList = new ArrayList<QsmOption>();
		for (int i = 0; i < 10; i++) {
			QsmOption option = new QsmOption();
			option.setOptionId("OPTION_ID_" + i);
			option.setQuestionId("789");
			option.setContent(QSM_CONTENT);
			option.setUserId(USER_ID);
			option.setShowOrder(i + 10f);
			qsmOptionList.add(option);
		}
		user.setQsmOptionList(qsmOptionList);

		boolean result = Dborm.insert(user);
		assertEquals(true, result);
	}

	public void testB10GetEntityCount() {
		initTestData();
		int count = Dborm.getEntityCount(LoginUser.class);
		assertEquals(1, count);
	}

	public void testB13GetCount() {
		initTestData();
		String sql = "SELECT COUNT(*) FROM login_user where user_id = ? ";
		String[] selectionArgs = new String[] { USER_ID };
		int count = Dborm.getCount(sql, selectionArgs);
		assertEquals(1, count);
	}

	public void testB15GetEntity() {
		initTestData();
		String sql = "SELECT * FROM login_user where user_id = ? ";
		String[] bindArgs = new String[] { USER_ID };
		LoginUser user = Dborm.getEntity(sql, bindArgs, LoginUser.class);
		assertEquals(USER_NAME, user.getUserName());
	}

	public void testB20GetEntitys() {
		initTestData();
		String sql = "SELECT * FROM qsm_option where user_id = ? ";
		String[] bindArgs = new String[] { USER_ID };
		List<QsmOption> userList = Dborm.getEntities(sql, bindArgs, QsmOption.class);
		assertEquals(10, userList.size());
		assertEquals(QSM_CONTENT, userList.get(2).getContent());
	}

	public void testC25GetCourse() {
		initTestData();
		String sql = "SELECT id, user_name, age FROM login_user where user_id = ? ";
		String[] selectionArgs = new String[] { USER_ID };
		SQLiteDatabase database = null;
		Cursor cursor = null;
		try {
			database = Dborm.getDatabase();
			cursor = database.rawQuery(sql, selectionArgs);
			cursor.moveToNext();// 一定要执行一下这句话，否则游标指向的不是第一条记录

			int columnIndex = cursor.getColumnIndex("user_name");// 根据字段名称获得字段的索引
			String userName = cursor.getString(columnIndex);// 再根据索引获得字段值
			assertEquals(USER_NAME, userName);
			int age = cursor.getInt(2);
			assertEquals(USER_AGE, age);
		} catch (Exception e) {
			LoggerUtilsDborm.error(e);
		} finally {
			Dborm.getDataBase().closeConn(database);
		}
	}

	public void testB26GetCourse() {
		initTestData();
		String sql = "SELECT option_id, content, show_order FROM qsm_option where user_id = ? ";
		String[] selectionArgs = new String[] { USER_ID };
		SQLiteDatabase database = null;
		Cursor cursor = null;
		try {
			database = Dborm.getDatabase();
			cursor = database.rawQuery(sql, selectionArgs);
			float i = 10;
			while (cursor.moveToNext()) {
				int columnIndex = cursor.getColumnIndex("content");// 根据字段名称获得字段的索引
				String content = cursor.getString(columnIndex);// 再根据索引获得字段值
				assertEquals(QSM_CONTENT, content);
				float showOrder = cursor.getFloat(2);
				assertEquals(i, showOrder);
				i++;
			}
		} catch (Exception e) {
			LoggerUtilsDborm.error(e);
		} finally {
			Dborm.getDataBase().closeConn(database);
		}
	}

	public void testB30GetEntitiesByExample() {
		initTestData();
		QsmOption qsmOption = new QsmOption();
		qsmOption.setUserId(USER_ID);
		List<QsmOption> userList = Dborm.getEntitiesByExample(qsmOption, true);
		assertEquals(10, userList.size());
		assertEquals(QSM_CONTENT, userList.get(2).getContent());
	}

	String myMapperContantTest = "append_message_test";
	public void testB35GetEntitys() {
		initTestData();
		String sql = "SELECT * FROM qsm_option where user_id = ? ";
		String[] bindArgs = new String[] { USER_ID };
		List<QsmOption> userList = Dborm.getEntities(sql, bindArgs, new MyMapper());
		assertEquals(10, userList.size());
		assertEquals(QSM_CONTENT + myMapperContantTest, userList.get(2).getContent());
	}

	
	class MyMapper implements Dborm.ResultMapper<QsmOption> {

		@Override
		public QsmOption map(Cursor cursor) {
			QsmOption qsmOption = new QsmOption();
			try {
				int contentColumnIndex = cursor.getColumnIndex("content");
				qsmOption.setContent(cursor.getString(contentColumnIndex) + myMapperContantTest);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return qsmOption;
		}
	}

}
