package cn.cocho.dborm.test.base;

import java.io.File;
import java.util.Date;

import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;
import cn.cocho.dborm.core.Dborm;
import cn.cocho.dborm.core.DbormInit;
import cn.cocho.dborm.util.LoggerUtilsDborm;
import cn.cocho.dborm.util.DbormContexts;
import cn.cocho.dborm.util.DbormDataBase;
import cn.cocho.dborm.utils.LoggerTools;
import cn.cocho.dborm.utils.MainActivity;
import cn.cocho.dborm.utils.domain.LoginUser;

public class DbormInitTest extends ActivityInstrumentationTestCase2<MainActivity> {

	public String dbPath;

	public DbormInitTest() {
		super("cn.cocho.dborm.test", MainActivity.class);
	}

	protected void setUp() throws Exception {
		super.setUp();
		initContexts();
		initDb();
	}

	public void initContexts() {
		dbPath = Environment.getExternalStorageDirectory() + File.separator + "tbc" + File.separator + "db" + File.separator
				+ "db_test.db";
		DbormContexts.showSql = true;
		DbormContexts.log = new LoggerTools();
		DbormContexts.dbVersion = 1;
		DbormContexts.setDbFilePath(dbPath);
	}

	public void initDb() {
		File oldFile = new File(DbormContexts.getDbFilePath());
		oldFile.delete();
		try {
			Dborm.setDataBase(new DbormDataBase());
			new DbormInit().initDb(this.getActivity());
		} catch (Exception e) {
			LoggerUtilsDborm.error(e);
		}

		if (DbormInit.dbSchemaHasUpdate) {
			System.out.println("数据库的数据结构本次发生了改变");
		}
		File file = new File(DbormContexts.getDbFilePath());
		assertEquals(true, file.exists());
	}

	public void testA20CheckTableIsExists() {
		LoginUser user = new LoginUser();
		user.setId("ID1");
		user.setUserId("USID1");
		user.setUserName("Tom");
		user.setAge(10);
		user.setBirthday(new Date());
		boolean insertRs = Dborm.insert(user);
		assertEquals(true, insertRs);

		LoginUser user1 = Dborm.getEntity("select * from login_user", null, LoginUser.class);
		boolean result = true;
		if (user1 == null) {
			result = false;
		}
		assertEquals(true, result);
	}

}
