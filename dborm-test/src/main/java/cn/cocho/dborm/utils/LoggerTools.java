package cn.cocho.dborm.utils;

import android.util.Log;
import cn.cocho.dborm.util.LoggerDborm;

/**
 * 日志处理类
 * 
 * @author HAIKANG SONG
 * @date 2013-4-17 下午3:07:28
 */
public class LoggerTools implements LoggerDborm {

	private String commonTarget = "cn.cocho.android";

	/**
	 * 调试信息
	 * 
	 * @param target
	 *            目标类路径
	 * @param msg
	 *            信息
	 * @author HAIKANG SONG
	 * @time 2013-4-22下午5:01:53
	 */
	public void debug(String target, String msg) {
		Log.d(target, msg);
	}

	/**
	 * 调试信息
	 * 
	 * @param msg
	 * @return void
	 * @author hsx
	 * @time 2013-6-17下午04:12:48
	 */
	public void debug(String msg) {
		Log.d(commonTarget, msg);

	}

	/**
	 * 提示
	 * 
	 * @param target
	 *            目标类路径
	 * @param msg
	 *            信息
	 * @author HAIKANG SONG
	 * @time 2013-4-22下午5:01:53
	 */
	public void warn(String target, String msg) {
		Log.w(target, msg);
	}

	/**
	 * 
	 * 提示
	 * 
	 * @param msg
	 * @return void
	 * @author hsx
	 * @time 2013-6-17下午04:13:42
	 */
	public void warn(String msg) {
		Log.w(commonTarget, msg);
	}

	/**
	 * 异常
	 * 
	 * @param target
	 *            目标类路径
	 * @param msg
	 *            信息
	 * @param e
	 *            异常对象
	 * @author HAIKANG SONG
	 * @time 2013-4-22下午5:02:21
	 */
	public void error(String target, String msg, Throwable e) {
		Log.e(target, msg, e);
	}

	/**
	 * 异常
	 * 
	 * @param target
	 *            目标类路径
	 * @param e
	 *            异常对象
	 * @author HAIKANG SONG
	 * @time 2013-4-22下午5:03:03
	 */
	public void error(String target, Throwable e) {
		Log.e(target, "", e);
	}

	/**
	 * 异常
	 * 
	 * @param e
	 * @return void
	 * @author hsx
	 * @time 2013-6-17下午04:14:30
	 */
	public void error(Throwable e) {
		Log.e(commonTarget, "", e);
	}

	/**
	 * 将异常对象转换为字符串
	 * 
	 * @param e
	 *            异常类对象
	 * @return
	 * @author HAIKANG SONG
	 * @time 2013-4-22下午5:03:31
	 */
	public String getExcetionMsg(Throwable e) {
		String msg = "";
		if (e != null) {
			StackTraceElement[] stackTrace = e.getStackTrace();
			StringBuilder errMsg = new StringBuilder();
			for (StackTraceElement element : stackTrace) {
				errMsg.append(element.toString());
				errMsg.append("\r\n");// 换行 每个个异常栈之间换行
			}
			msg = errMsg.toString();
		}
		return msg;
	}

}
