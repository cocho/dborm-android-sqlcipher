package cn.cocho.dborm.utils.domain;

public class QsmOption{

	/**
	 * 问题选项id
	 */
	private String optionId;
	
	/**
	 * 关联到的问题id
	 */
	private String questionId;
	
	/**
	 * 问题选项的内容
	 */
	private String content;
	
	/**
	 * 选项的排序
	 */
	private Float showOrder;
	
	/**
	 * 冗余字段，用于标示当前题干的附件个数。
	 */
	private Integer attachmentCount = 0;
	
	private String userId;
	
//	@Relation("附件")
//	private List<QsmAttachment> attachments;

	public String getOptionId() {
		return optionId;
	}

	public void setOptionId(String optionId) {
		this.optionId = optionId;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Float getShowOrder() {
		return showOrder;
	}

	public void setShowOrder(Float showOrder) {
		this.showOrder = showOrder;
	}

	public Integer getAttachmentCount() {
		return attachmentCount;
	}

	public void setAttachmentCount(Integer attachmentCount) {
		this.attachmentCount = attachmentCount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	
}
